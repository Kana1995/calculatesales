package jp.alhinc.nakajima_kana.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalculateSales {

	public static void main(String[] args) {

		//Mapを宣言して値を保持する
		Map<String,String> branchNames = new HashMap<>(); //支店コード＆支店名のデータを保持
		Map<String,Long>   branchSales = new HashMap<>();   //支店コード＆売上額のデータを保持
		Map<String,String> commodityNames = new HashMap<>();   //商品コード＆商品名のデータを保持
		Map<String,Long>   commoditySales = new HashMap<>();   //商品コード＆売上額のデータを保持

		/*
		 * コマンドライン引数が渡されていない場合は、
		 * エラーメッセージ「予期せぬエラーが発生しました」を表示し、処理を終了する。
		 */
		if(args.length != 1) {
			System.out.println("予期せぬエラーが発生しました");
			return;
		}

		//メソッド分け
		//支店定義ファイルを読み込む

		if(!inputFile(args[0], "branch.lst", "支店",
				  branchNames, branchSales, "^[0-9]{3}")) {
			return;
		}

		//商品定義ファイルを読み込む

		if(!inputFile(args[0], "commodity.lst", "商品",
				  commodityNames, commoditySales, "^[0-9a-zA-Z]{8}")){
			return;
		}

		//Fileクラスのオブジェクトを生成し、対象のディレクトリを指定
		//listFileを使用してファイル一覧を取得
		File[] files = new File(args[0]).listFiles();
		List<File> rcdFiles = new ArrayList<>();

		for(int i = 0; i < files.length; i++) {
			//ファイルなのか確認
			if(files[i].isFile() && files[i].getName().matches("^[0-9]{8}.+rcd$")){
				rcdFiles.add(files[i]);
			}
		}
		/*
		Windowsの場合はディレクトリからファイルを読み込んだ場合、
		昇順にソートされた状態で読み込まれますが、
		MacOSの場合は異なります。OS問わず、正常に動作させるためには連番チェックを行う前に、
		売上ファイルを保持しているListをソートする必要があります。
		*/

		Collections.sort(rcdFiles);

		/*
		売上ファイルが連番になっていない場合、
		エラーメッセージ「売上ファイル名が連番になっていません」を表示し、処理を終了する。
		*/
		for(int i = 0; i < rcdFiles.size() - 1; i++) {

			int former = Integer.parseInt(rcdFiles.get(i).getName().substring(0, 8)); //i番目のファイル
			int latter = Integer.parseInt(rcdFiles.get(i + 1).getName().substring(0, 8)); //i+1番目のファイル

			if ((latter - former) != 1) {
				System.out.println("売上ファイル名が連番になっていません");
				return;
			}
		}

		//売上ファイルのファイル名を表示
		//System.out.println("売上ファイル名 =>" + rcdFiles);

		//支店コード、売上額を抽出する
		//抽出した売上額を該当する支店の合計金額にそれぞれ加算する。
		//それを売上ファイルの数だけ繰り返す
		BufferedReader br = null;
		for(int i = 0; i < rcdFiles.size(); i++) {
			try {
				//FileReaderクラスでrcdFilesを読み込む
				FileReader fr = new FileReader(rcdFiles.get(i));
				br = new BufferedReader(fr);

				String rcdFileLine;

				//支店コードと売上額の格納先List
				List<String> rcdDate = new ArrayList<>();

				while((rcdFileLine = br.readLine()) != null){
					rcdDate.add(rcdFileLine);
				}

				/*
				 * 売上ファイルの中身が3行ではなかった場合は、
				 * エラーメッセージ「<該当ファイル名>のフォーマットが不正です」と表示し、処理を終了する。
				 */

				if(rcdDate.size() != 3){
					System.out.println(rcdFiles.get(i).getName() + "のフォーマットが不正です");
					return;
				}

				String rcdBranchCode = rcdDate.get(0);  //支店コード抽出
				String rcdCommodityCode = rcdDate.get(1); //商品コード抽出
				String rcdBranchSale = rcdDate.get(2);  //売上額抽出

				/*
				 * 売上ファイルの売上金額が数字ではなかった場合は、
				 * エラーメッセージ「予期せぬエラーが発生しました」を表示し、処理を終了する。
				 */
				if(!rcdBranchSale.matches("^[0-9]+$")){
					System.out.println("予期せぬエラーが発生しました");
					return;
				}

				/*
				売上ファイルの支店コードが支店定義ファイルに該当しなかった場合は、
				エラーメッセージ「<該当ファイル名>の支店コードが不正です」と表示し、処理を終了する。
				*/
				if(!branchSales.containsKey(rcdBranchCode)){
					System.out.println(rcdFiles.get(i).getName() + "の支店コードが不正です");
					return;
				}

				/*
				売上ファイルの商品コードが商品定義ファイルに該当しなかった場合は、
				エラーメッセージ「<該当ファイル名>の商品コードが不正です」と表示し、処理を終了する。
				*/
				if(!commoditySales.containsKey(rcdCommodityCode)){
					System.out.println(rcdFiles.get(i).getName() + "の商品コードが不正です");
					return;
				}

				Long fileSale = Long.parseLong(rcdBranchSale);

				//抽出した売上額を該当する支店の合計金額にそれぞれ加算
				Long saleAmount = branchSales.get(rcdBranchCode) + fileSale; //加算

				/*
				集計した売上金額が10桁を超えた場合、
				エラーメッセージ「合計金額が10桁を超えました」を表示し、処理を終了する。
				*/
				if(saleAmount >= 1000000000L) {
					System.out.println("合計金額が10桁を超えました");
					return;
				}

				//Mapへ格納
				branchSales.put(rcdBranchCode, saleAmount);

				Long fileCommoditySale = Long.parseLong(rcdBranchSale);

				//抽出した売上額を該当する商品の合計金額にそれぞれ加算
				Long comodittySaleAmount = branchSales.get(rcdBranchCode) + fileCommoditySale;

				/*
				集計した売上金額が10桁を超えた場合、
				エラーメッセージ「合計金額が10桁を超えました」を表示し、処理を終了する。
				*/
				if(comodittySaleAmount >= 1000000000L){
					System.out.println("合計金額が10桁を超えました");
					return;
				}

				//Mapへ格納
				commoditySales.put(rcdCommodityCode, saleAmount);

			}catch(IOException e){
				System.out.println("予期せぬエラーが発生しました");
				return;
			}finally {
				if(br != null){
					try {
						br.close();
					}catch(IOException e){
						System.out.println("予期せぬエラーが発生しました");
						return;
					}
				}
			}
		}

		//メソッド分け
		if(!outputFile(args[0],"branch.out", branchNames, branchSales)){
			return;
		}

		if(!outputFile(args[0], "commodity.out", commodityNames, commoditySales)){
			return;
		}
	}

	//メソッド分け
	//ファイルの読み込みメソッド
	public static boolean inputFile(String path, String fileName, String difinition,
			                Map<String, String> names, Map<String, Long>sales, String regex){

		BufferedReader br = null;
		try{
			//定義ファイルを読み込む
			File file = new File(path, fileName);

			if(!file.exists()){
				System.out.println(difinition + "定義ファイルが存在しません");
				return false;
			}

			//FileReaderクラスでファイルを読み込む
			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);

			String line;

			//ファイルを1行ずつ読み込む
			while((line = br.readLine()) != null){
				//行をカンマ区切りで配列に変換
				String[] data = line.split(",");
				names.put(data[0], data[1]);
				sales.put(data[0], 0L);

				if(data.length != 2 || (!data[0].matches(regex))){
					System.out.println(difinition + "定義ファイルのフォーマットが不正です");
					return false;
				}
			}
		}catch(IOException e){
			System.out.println("予期せぬエラーが発生しました");
			return false;
		}finally {
			if(br != null) {
				try {
					br.close();
				}catch(IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}
		}
		return true;

	}

	//コマンドライン引数で指定されたディレクトリに支店別/商品別集計ファイルを作成

	public static boolean outputFile(String path, String fileName,
			                Map<String, String> names, Map<String, Long> sales){

		BufferedWriter bw = null;
		try {
			//FileWriter と BufferedWriterでファイルを書き込む
			File file = new File(path, fileName);

			FileWriter fw = new FileWriter(file);
	        bw = new BufferedWriter(fw);

	        //keyを出してからkeyを元にvalueも格納する
			for(String key: sales.keySet()) {
				String valueNames = names.get(key);
				long valueSales = sales.get(key);

				bw.write(key + "," + valueNames + "," + valueSales);
				bw.newLine();
			}
		}catch(IOException e){
			System.out.println("予期せぬエラーが発生しました");
			return false;
		}finally{
			if(bw != null) {
				try {
					bw.close();
				}catch(IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}
		}
		return true;
	}


}





